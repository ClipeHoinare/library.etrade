﻿using System;
using Library.Etrade.Domain;
using Library.Etrade.Repository;
using Library.Etrade.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library.Etrade.Tests.FunctionalTest
{
    [TestClass]
    public class EtradeAccountServiceFixture
    {
        [TestMethod]
        public void EtradeAccount_GetAccounts_Sandbox()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeAccountsService(sessionToken);

            var accounts = service.GetAccounts();
        }

        [TestMethod]
        public void EtradeAccount_GetAccounts_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeAccountsService(sessionToken);

            var accounts = service.GetAccounts();
        }
    }
}
