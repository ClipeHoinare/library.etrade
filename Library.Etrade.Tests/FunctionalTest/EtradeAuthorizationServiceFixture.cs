﻿using System;
using Library.Etrade.Domain;
using Library.Etrade.Repository;
using Library.Etrade.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library.Etrade.Tests.FunctionalTest
{
    [TestClass]
    public class EtradeAuthorizationFixture
    {
        [TestMethod]
        public void EtradeAuthorization_GetSessionToken_Sandisk()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeAuthorizationService(sessionToken);

            var token = service.GetSessionToken();
        }

        [TestMethod]
        public void EtradeAuthorization_RenewAccessToken_Sandisk()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeAuthorizationService(sessionToken);

            var token = service.GetSessionToken();

            service.RenewAccessToken();
        }

        [TestMethod]
        public void EtradeAuthorization_RevokeAccessToken_Sandisk()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeAuthorizationService(sessionToken);

            var token = service.GetSessionToken();

            service.RevokeAccessToken();
        }

        [TestMethod]
        public void EtradeAuthorization_GetSessionToken_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeAuthorizationService(sessionToken);

            var token = service.GetSessionToken();
        }

        [TestMethod]
        public void EtradeAuthorization_RenewAccessToken_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeAuthorizationService(sessionToken);

            var token = service.GetSessionToken();

            service.RenewAccessToken();
        }

        [TestMethod]
        public void EtradeAuthorization_RevokeAccessToken_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeAuthorizationService(sessionToken);

            var token = service.GetSessionToken();

            service.RevokeAccessToken();
        }
    }
}
