﻿using Library.Domain.TradePortal;
using Library.Etrade.Domain;
using Library.Etrade.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Library.Etrade.Tests.FunctionalTest
{
    [TestClass]
    public class EtradeMarketServiceFixture
    {
        [TestMethod]
        public void EtradeMarket_GetMarketQuotes_Sandbox()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeMarketService(sessionToken);

            var bucket = service.GetMarketQuotes("XOM,BP,MRO");
        }

        [TestMethod]
        public void EtradeMarket_GetLookUpProduct_Sandbox()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeMarketService(sessionToken);

            var optionExpireDates = service.GetLookUpProduct("X");
        }

        [TestMethod]
        public void EtradeMarket_GetOptionChains_Sandbox()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeMarketService(sessionToken);

            TradePortalContract contractBucket = new TradePortalContract();
            contractBucket.Symbol = "XOM";

            var optionExpireDates = service.GetOptionChains(contractBucket);
        }

        [TestMethod]
        public void EtradeMarket_GetOptionExpireDates_Sandbox()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Sandbox");

            var service = new EtradeMarketService(sessionToken);

            var bucket = service.GetOptionExpireDates("XOM");
        }

        [TestMethod]
        public void EtradeMarket_GetMarketQuotes_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeMarketService(sessionToken);

            var bucket = service.GetMarketQuotes("XOM,BP,IBM");
        }

        [TestMethod]
        public void EtradeMarket_GetLookUpProduct_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeMarketService(sessionToken);

            var optionExpireDates = service.GetLookUpProduct("X");
        }

        [TestMethod]
        public void EtradeMarket_GetOptionChains_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeMarketService(sessionToken);

            TradePortalContract contractBucket = new TradePortalContract();
            contractBucket.Symbol = "XOM";
            contractBucket.ComboLegs = new List<TradePortalComboLeg>
            {
                new TradePortalComboLeg
                {
                    LastTradeDateOrContractMonth="20190517"
                }
            };

            var optionExpireDates = service.GetOptionChains(contractBucket);
        }

        [TestMethod]
        public void EtradeMarket_GetOptionExpireDates_Live()
        {
            OauthSessionToken sessionToken = new OauthSessionToken("Live");

            var service = new EtradeMarketService(sessionToken);

            var bucket = service.GetOptionExpireDates("XOM");
        }
    }
}
