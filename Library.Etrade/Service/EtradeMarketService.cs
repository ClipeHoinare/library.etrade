﻿//using Library.Domain.TradePortal;
using Library.Domain.TradePortal;
using Library.Etrade.Domain;
using Library.Etrade.Repository;
using System.Collections.Generic;


namespace Library.Etrade.Service
{
    public class EtradeMarketService : EtradeBaseService
    {
        #region constructor/destructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="habitat">habitatInfo object and current session</param>
        public EtradeMarketService(OauthSessionToken sessionToken)
        {
            _sessionToken = sessionToken;

            if (!_sessionToken.HasCredentials())
            {
                _sessionToken = (new OAuthProcessor(_sessionToken)).GetSessionToken();
            }
        }

        public void Dispose()
        {
            _sessionToken.Dispose();
        }

        #endregion

        public MarketQuotesBucket GetMarketQuotes(string symbols)
        {
            var marketRepository = new MarketRepository(_sessionToken);

            var bucket = marketRepository.GetMarketQuotes(symbols);

            return bucket;
        }

        public LookUpProductBucket GetLookUpProduct(string search)
        {
            var marketRepository = new MarketRepository(_sessionToken);

            var bucket = marketRepository.GetLookUpProduct(search);

            return bucket;
        }

        public List<TradePortalOptionChain> GetOptionChains(TradePortalContract contractBucket)
        {
            var etradeRequest = GenerateRequestOptionChains(contractBucket);

            var marketRepository = new MarketRepository(_sessionToken);

            var bucket = marketRepository.GetOptionChains(etradeRequest);

            List<TradePortalOptionChain> optionChains = bucket.ToOptionChains();

            return optionChains;
        }

        private EtradeRequestOptionChains GenerateRequestOptionChains(TradePortalContract contractBucket)
        {
            var request = new EtradeRequestOptionChains();

            request.symbol = contractBucket.Symbol;
            request.expiryYear = contractBucket.ComboLegs[0].LastTradeDateOrContractMonth.Substring(0, 4);
            request.expiryMonth = contractBucket.ComboLegs[0].LastTradeDateOrContractMonth.Substring(4, 2);
            request.expiryDay = contractBucket.ComboLegs[0].LastTradeDateOrContractMonth.Substring(6, 2);
            request.optionCategory = "ALL";
            request.optionCategory = "ALL";
            request.priceType = "ALL";
            if (contractBucket.ComboLegs[0].Right == "C")
                request.chainType = "CALL";
            else if (contractBucket.ComboLegs[0].Right == "P")
                request.chainType = "PUT";
            else
                request.chainType = "CALLPUT";

            return request;
        }

        public List<TradePortalExpireDates> GetOptionExpireDates(string symbol)
        {
            var marketRepository = new MarketRepository(_sessionToken);

            var bucket = marketRepository.GetOptionExpireDates(symbol);

             var list = bucket.ToExpireDates();

            return list;
        }
    }
}








