﻿//using Library.Domain.TradePortal;
using Library.Domain.TradePortal;
using Library.Etrade.Domain;
using Library.Etrade.Repository;
using System.Collections.Generic;

namespace Library.Etrade.Service
{
    public class EtradeAccountsService : EtradeBaseService
    {
        public OauthSessionToken SessionToken
        {

            get
            {
                return _sessionToken;
            }

            set => _sessionToken = value;
        }

        #region constructor/destructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="habitat">habitatInfo object and current session</param>
        public EtradeAccountsService(OauthSessionToken sessionToken)
        {
            _sessionToken = sessionToken;

            if (!_sessionToken.HasCredentials())
            {
                _sessionToken = (new OAuthProcessor(sessionToken)).GetSessionToken();
            }
        }

        public void Dispose()
        {
            _sessionToken.Dispose();
        }

        #endregion
        public List<TradePortalAccount> GetAccounts()
        {
            var accountRepository = new AccountRepository(_sessionToken);

            var accounts = accountRepository.GetListAccounts();

            return accounts;
        }
    }
}
