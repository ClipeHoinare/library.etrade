﻿using Library.Etrade.Domain;
using Library.Etrade.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Etrade.Service
{
    public class EtradeBaseService
    {
        protected OauthSessionToken _sessionToken { get; set; } = null;

        protected EtradeLogger Logger { get; set; } = new EtradeLogger();
    }
}
