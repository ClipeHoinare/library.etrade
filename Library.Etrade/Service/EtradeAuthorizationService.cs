﻿using Library.Etrade.Domain;
using Library.Etrade.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Library.Etrade.Service
{
    public class EtradeAuthorizationService : EtradeBaseService
    {
        #region constructor/destructor

        public EtradeAuthorizationService(OauthSessionToken sessionToken)
        {
            _sessionToken = sessionToken;
        }

        #endregion

        public OauthSessionToken GetSessionToken()
        {
            _sessionToken = (new OAuthProcessor(_sessionToken)).GetSessionToken();

            return _sessionToken;
        }
        public bool RenewAccessToken()
        {
            var result = (new OAuthProcessor(_sessionToken)).RenewAccessToken();

            return result;
        }
        public bool RevokeAccessToken()
        {
            var result = (new OAuthProcessor(_sessionToken)).RevokeAccessToken();

            return result;
        }
    }
}




