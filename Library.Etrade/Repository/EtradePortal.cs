﻿//using Library.Etrade.Domain;
//using RestSharp;
//using RestSharp.Authenticators;
//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Text;

//namespace Library.Etrade.Repository
//{
//    /// <summary>
//    /// RestSharp documentation: https://github.com/restsharp/RestSharp/wiki
//    /// </summary>
//    public class EtradePortal
//    {
//        RestClient restClient;
//        OauthSessionToken _sessionToken;

//        internal EtradePortal(OauthSessionToken session)
//        {
//            _sessionToken = session;
//            restClient = new RestClient();
//        }

//        public OauthRequestToken GetRequestToken()
//        {
//            var requestToken = new OauthRequestToken();
//            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
//            restClient.Authenticator = OAuth1Authenticator.ForRequestToken(_sessionToken.consumer_key, _sessionToken.consumer_secret, "oob");

//            RestRequest restRequest = new RestRequest("oauth/request_token", Method.GET);

//            IRestResponse response = restClient.Execute(restRequest);

//            if (response.StatusCode != System.Net.HttpStatusCode.OK)
//            {
//                requestToken.oauth_token = null;
//                requestToken.oauth_token_secret = null;
//                return null;
//            }

//            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(response.Content);

//            requestToken.oauth_token = queryString["oauth_token"];
//            requestToken.oauth_token_secret = queryString["oauth_token_secret"];

//            return requestToken;
//        }

//        public OauthSessionToken GetAccessToken(OauthRequestToken requestToken)
//        {
//            restClient.Authenticator = OAuth1Authenticator.ForAccessToken(_sessionToken.consumer_key, _sessionToken.consumer_secret, requestToken.oauth_token, requestToken.oauth_token_secret , requestToken.oauth_verifier);

//            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
//            RestRequest restRequest = new RestRequest("oauth/access_token", Method.GET);
//            IRestResponse irestResponse = restClient.Execute(restRequest);

//            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(irestResponse.Content);

//            _sessionToken.oauth_token = queryString["oauth_token"];
//            _sessionToken.oauth_token_secret = queryString["oauth_token_secret"];

//            return _sessionToken;
//        }

//        public bool RenewAccessToken()
//        {
//            restClient.Authenticator = OAuth1Authenticator.ForAccessTokenRefresh(_sessionToken.consumer_key, _sessionToken.consumer_secret, _sessionToken.oauth_token, _sessionToken.oauth_token_secret, null);

//            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
//            RestRequest restRequest = new RestRequest("oauth/renew_access_token", Method.GET);
//            IRestResponse irestResponse = restClient.Execute(restRequest);

//            var contentString = irestResponse.Content;
//            var result = contentString.Trim().Equals("Access Token has been renewed");

//            return result;
//        }

//        public bool RevokeAccessToken()
//        {
//            restClient.Authenticator = OAuth1Authenticator.ForAccessTokenRefresh(_sessionToken.consumer_key, _sessionToken.consumer_secret, _sessionToken.oauth_token, _sessionToken.oauth_token_secret, null);

//            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
//            RestRequest restRequest = new RestRequest("oauth/revoke_access_token", Method.GET);
//            IRestResponse irestResponse = restClient.Execute(restRequest);

//            var contentString = irestResponse.Content;
//            var result = contentString.Trim().Equals("Revoked Access Token");

//            return result;
//        }

//        public string GetStringContent(string relativePath)
//        {
//            restClient.BaseUrl = new Uri(_sessionToken.ApiBaseUrl);
//            restClient.Authenticator = OAuth1Authenticator.ForProtectedResource(_sessionToken.consumer_key, _sessionToken.consumer_secret, _sessionToken.oauth_token, _sessionToken.oauth_token_secret);

//            RestRequest restRequest = new RestRequest(relativePath, Method.GET);

//            IRestResponse irestResponse = restClient.Execute(restRequest);

//            return irestResponse.Content;
//        }
//    }
//}
