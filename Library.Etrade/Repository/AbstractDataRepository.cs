﻿using Library.Etrade.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Etrade.Repository
{
    internal abstract class AbstractDataRepository
    {
        protected OauthSessionToken _sessionToken { set; get; } = null;
        protected OAuthProcessor _oauthProcessor { set; get; } = null;
        protected EtradeLogger _logger { set; get; } = null;

        public AbstractDataRepository(OauthSessionToken sessionToken)
        {
            _sessionToken = sessionToken;
            _logger = new EtradeLogger();
            _oauthProcessor = new OAuthProcessor(_sessionToken);
        }
    }
}
