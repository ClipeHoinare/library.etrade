﻿using Library.Etrade.Domain;
using System.Text;

namespace Library.Etrade.Repository
{
    internal class MarketRepository : AbstractDataRepository
    {
        public MarketRepository(OauthSessionToken sessionToken) : base (sessionToken)
        {
        }

        internal MarketQuotesBucket GetMarketQuotes(string symbols)
        {
            string relativePath = string.Format("market/quote/{0}?detailFlag=ALL", symbols);

            var jsonString = _oauthProcessor.GetStringContent(relativePath);

            var marketQuotes = MarketQuotesBucket.FromJson(jsonString);

            return marketQuotes;
        }

        internal LookUpProductBucket GetLookUpProduct(string search)
        {
            string relativePath = string.Format("market/lookup/{0}", search);

            var jsonString = _oauthProcessor.GetStringContent(relativePath);

            var lookUpProductBucket = LookUpProductBucket.FromJson(jsonString);

            return lookUpProductBucket;
        }

        internal OptionChainsBucket GetOptionChains(EtradeRequestOptionChains requestOptionChains)
        {
            string relativePath = GenerateRelativePathForOptionChains(requestOptionChains);

            var jsonString = _oauthProcessor.GetStringContent(relativePath);

            var optionChainsBucket = OptionChainsBucket.FromJson(jsonString);

            return optionChainsBucket;
        }

        private string GenerateRelativePathForOptionChains(EtradeRequestOptionChains request)
        {
            StringBuilder builder = new StringBuilder();
            //symbol={0}&expiryYear={1}&expiryMonth={2}&&expiryDay={3}&optionCategory={4}&priceType={5}
            //&chainType ={0}
            //&strikePriceNear=&noOfStrikes=
            //defaults: includeWeekly=false&skipAdjusted=true&priceType=ATNM&optionCategory=STANDARD

            builder.Append(string.Format("market/optionchains?symbol={0}&expiryYear={1}&expiryMonth={2}&&expiryDay={3}&optionCategory={4}&priceType={5}", 
                request.symbol, request.expiryYear, request.expiryMonth, request.expiryDay, request.optionCategory, request.priceType));
            if (request.chainType != null)
                builder.Append(string.Format("&chainType={0}", request.chainType));

            return builder.ToString();
        }

        internal OptionExpireDatesBucket GetOptionExpireDates(string symbol)
        {
            string relativePath = string.Format("market/optionexpiredate?symbol={0}", symbol);

            var jsonString = _oauthProcessor.GetStringContent(relativePath);

            var optionExpireDatesBucket = OptionExpireDatesBucket.FromJson(jsonString);

            return optionExpireDatesBucket;
        }

    }
}
