﻿//using Library.Domain.Configuration;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Library.Etrade.Domain
//{
//    public class EtradeHabitatInfo
//    {
//        public string EtradeUser = ApplicationConfiguration.AppSettings.EtradeUser;
//        public string EtradePass = ApplicationConfiguration.AppSettings.EtradePass;
//        public string consumer_key = ApplicationConfiguration.AppSettings.ConsumerKey;
//        public string consumer_secret = ApplicationConfiguration.AppSettings.ConsumerSecret;

//        // Authorization links
//        public string OauthBaseUrl = "https://api.etrade.com/";
//        public string RequestTokenUrl = "https://api.etrade.com/oauth/request_token";
//        public string AuthorizeUrl = "HTTPS://us.etrade.com/e/t/etws/authorize?key={0}&token={1}";
//        public string AccessTokenUrl = "https://api.etrade.com/oauth/access_token";
//        public string RenewAccessToken = "https://api.etrade.com/oauth/renew_access_token";
//        public string RevokeAccessToken = "https://api.etrade.com/oauth/revoke_access_token";

//        //Accounts lists
//        public string ApiBaseUrl = " https://apisb.etrade.com/v1/";
//        public string ListAccountsUrl = "accounts/list.json";

//        internal EtradeHabitatFlag Habitat { get; set; }

//        public EtradeHabitatInfo(EtradeHabitatFlag habitat = EtradeHabitatFlag.Sandbox)
//        {
//            Habitat = habitat;

//            if(habitat.Equals(EtradeHabitatFlag.Live))
//            {
//                //continue;
//            }
//        }
//    }
//    public enum EtradeHabitatFlag
//    {
//        Sandbox = 0,
//        Live = 1
//    }
//}
