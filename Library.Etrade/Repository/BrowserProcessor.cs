﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Library.Etrade.Repository
{
    /// <summary>
    /// HttpWebRequest/HttpWebResponse extension that keeps coockies from one call to next call, if you reuse the same object
    /// </summary>
    internal class BrowserProcessor
    {
        protected Dictionary<string, string> _cookies = new Dictionary<string, string>();
        public string HeaderLocation { get; set; } = string.Empty;
        public CookieContainer CookieContainer { get; set; } = new CookieContainer();

        public BrowserProcessor() : this(new CookieContainer())
        {
        }

        public BrowserProcessor(CookieContainer container)
        {
            this.CookieContainer = container;
            _cookies = new Dictionary<string, string>();
        }

        public string GetStringFromUrl(string urlRequest)
        {
            var result = string.Empty;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlRequest);
            httpWebRequest.Method = "GET";

            var response = httpWebRequest.GetResponse();

            var characterSet = ((HttpWebResponse)response).CharacterSet;
            var responseEncoding = characterSet == ""
                ? Encoding.UTF8
                : Encoding.GetEncoding(characterSet ?? "utf-8");
            var responsestream = response.GetResponseStream();

            using (responsestream)
            {
                var reader = new StreamReader(responsestream, responseEncoding);
                result = reader.ReadToEnd();
            }

            return result;
        }

        public string DownloadString(Uri uri)
        {
            var result = string.Empty;
            CookieContainer cookieJar = new CookieContainer();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Method = "GET";
            httpWebRequest.CookieContainer = cookieJar;

            // add cookies if exists
            if (_cookies != null && _cookies.Count > 0)
            {
                for (int index = 0; index < _cookies.Count; index++)
                {
                    KeyValuePair<string, string> item = _cookies.ElementAt(index);
                    cookieJar.Add(new Cookie(item.Key, item.Value) { Domain = uri.Host });
                }
            }

            var response = httpWebRequest.GetResponse();

            var characterSet = ((HttpWebResponse)response).CharacterSet;
            var responseEncoding = characterSet == ""
                ? Encoding.UTF8
                : Encoding.GetEncoding(characterSet ?? "utf-8");
            var responsestream = response.GetResponseStream();
            if (responsestream == null)
            {
                throw new ArgumentNullException(nameof(characterSet));
            }
            using (responsestream)
            {
                var reader = new StreamReader(responsestream, responseEncoding);
                result = reader.ReadToEnd();
            }

            ReadCookies((HttpWebRequest)httpWebRequest, (HttpWebResponse)response);

            return result;
        }

        public string UploadValues(Uri requestUri, NameValueCollection valueCollection)
        {
            var result = string.Empty;

            Dictionary<string, string> postList = new Dictionary<string, string>();

            foreach (string key in valueCollection)
            {
                postList.Add(key, valueCollection[key]);
            }

            Dictionary<string, string> cookieList = _cookies;


            var ret = SitePost(requestUri.AbsoluteUri, postList, cookieList);
            result = ret.Item1;

            return result;
        }

        protected Tuple<string, Dictionary<string, string>> SitePost(string url, Dictionary<string, string> postList, Dictionary<string, string> cookieList)
        {
            string toReturn = string.Empty;
            Dictionary<string, string> responseCookies = new Dictionary<string, string>();
            Exception exToThrow = null;
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                byte[] retBuff = new byte[0];

                //prepare request
                Uri uri = new Uri(url);

                CookieContainer cookieJar = new CookieContainer();
                request = (HttpWebRequest)WebRequest.Create(uri);
                request.CookieContainer = cookieJar;
                request.Timeout = 55000;
                request.Accept = "*/*";
                request.AllowWriteStreamBuffering = false;
                //wr.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.KeepAlive = true;
                //Tell server that you are in line with the best, otherwise the crazy server will tell you to upgrade "poftim cultura", ... not(Borat)
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763";
                request.AllowAutoRedirect = true;

                // add cookies if exists
                if (cookieList != null && cookieList.Count > 0)
                {
                    request.CookieContainer = new CookieContainer();
                    for (int index = 0; index < cookieList.Count; index++)
                    {
                        KeyValuePair<string, string> item = cookieList.ElementAt(index);
                        request.CookieContainer.Add(new Cookie(item.Key, item.Value) { Domain = uri.Host });
                    }
                }

                string postData = string.Empty;
                // add postdata if exists
                if (postList != null && postList.Count > 0)
                {
                    // prepare post data
                    int count = postList.Count;
                    for (int index = 0; index < count; index++)
                    {
                        KeyValuePair<string, string> item = postList.ElementAt(index);
                        postData = postData + (postData.Length <= 0 ? "" : "&") + Uri.EscapeDataString(item.Key) + "=" + Uri.EscapeDataString(item.Value);
                    }

                }
                // write post data to the request object
                byte[] sendBuff = System.Text.ASCIIEncoding.ASCII.GetBytes(postData);
                request.ContentLength = sendBuff.Length;
                Stream outStream = request.GetRequestStream();
                outStream.Write(sendBuff, 0, sendBuff.Length);
                outStream.Flush();
                outStream.Close();

                //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback ( delegate { return true; } );
                //get response
                response = (HttpWebResponse)request.GetResponse();

                // Now look to see if it's a redirect
                if ((int)response.StatusCode == 302)
                {
                    HeaderLocation = response.Headers["Location"];
                }

                // read response
                Stream str = response.GetResponseStream();
                MemoryStream ms = new MemoryStream();
                retBuff = new byte[8192];

                int len = 0;
                len = str.Read(retBuff, 0, retBuff.Length);
                while (len > 0)
                {
                    ms.Write(retBuff, 0, len);
                    len = str.Read(retBuff, 0, retBuff.Length);
                }
                str.Close();
                retBuff = ms.ToArray();
                toReturn = System.Text.ASCIIEncoding.ASCII.GetString(retBuff);

                ms.Close();

                foreach (Cookie c in cookieJar.GetCookies(request.RequestUri))
                {
                    responseCookies.Add(c.Name, c.Value);
                }

                ReadCookies(request, response);
            }
            catch (Exception ex)
            {
                exToThrow = ex;
            }

            // clean after you
            try
            {
                if (response != null)
                    response.Close();
            }
            catch { }

            try
            {
                if (request != null)
                    request.Abort();
            }
            catch { }

            // if error encounter throw it again
            if (exToThrow != null)
                throw exToThrow;

            return new Tuple<string, Dictionary<string, string>>(toReturn, responseCookies);
        }

        protected void ReadCookies(HttpWebRequest request, HttpWebResponse response)
        {
            if (response != null)
            {
                CookieContainer cookies = request.CookieContainer;

                foreach (Cookie cookie in cookies.GetCookies(request.RequestUri))
                {
                    if (_cookies.Any(p => p.Key == cookie.Name))
                    {
                        _cookies[cookie.Name] = cookie.Value;
                    }
                    else
                    {
                        _cookies.Add(cookie.Name, cookie.Value);
                    }

                }
            }

            if (response != null)
            {
                CookieCollection cookies = response.Cookies;

                foreach (Cookie cookie in cookies)
                {
                    if (_cookies.Any(p => p.Key == cookie.Name))
                    {
                        _cookies[cookie.Name] = cookie.Value;
                    }
                    else
                    {
                        _cookies.Add(cookie.Name, cookie.Value);
                    }

                }
            }

        }
    }
}
