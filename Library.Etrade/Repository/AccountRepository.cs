﻿using Library.Domain.TradePortal;
using Library.Etrade.Domain;
using System.Collections.Generic;

namespace Library.Etrade.Repository
{
    internal class AccountRepository : AbstractDataRepository
    {
        public AccountRepository(OauthSessionToken sessionToken) : base(sessionToken)
        {
        }

        //https://apisb.etrade.com/docs/api/account/api-account-v1.html
        internal List<TradePortalAccount> GetListAccounts()
        {
            var jsonString = _oauthProcessor.GetStringContent("accounts/list");

            var accounts = AccountsBucket.FromJson(jsonString);

            //var jsonAccounts = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonString);

            var accountList = accounts.ToAccountList();


            return accountList;
        }
    }
}
