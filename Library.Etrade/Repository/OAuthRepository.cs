﻿using HtmlAgilityPack;
using Library.Etrade.Domain;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Library.Etrade.Repository
{
    internal class OAuthProcessor
    {
        RestClient restClient;
        private OauthSessionToken _sessionToken = null;

        public OAuthProcessor(OauthSessionToken sessionToken)
        {
            _sessionToken = sessionToken;
            restClient = new RestClient();
        }

        public OauthSessionToken GetSessionToken()
        {
            OauthRequestToken requestToken = null;
            for (int i = 0; i < 5; i++)
            {
                requestToken = GetRequestToken();
                if (requestToken == null)
                    Thread.Sleep(1 * 1000);
                else
                    break;
            }

            requestToken.oauth_verifier = GetAuthorizationCode(requestToken);

            _sessionToken = GetAccessToken(requestToken);

            return _sessionToken;
        }

        public OauthRequestToken GetRequestToken()
        {
            var requestToken = new OauthRequestToken();
            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
            restClient.Authenticator = OAuth1Authenticator.ForRequestToken(_sessionToken.ConsumerKey, _sessionToken.ConsumerSecret, "oob");

            RestRequest restRequest = new RestRequest("oauth/request_token", Method.GET);

            IRestResponse response = restClient.Execute(restRequest);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                //TODO: Log error
                requestToken.oauth_token = null;
                requestToken.oauth_token_secret = null;
                return null;
            }

            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(response.Content);

            requestToken.oauth_token = queryString["oauth_token"];
            requestToken.oauth_token_secret = queryString["oauth_token_secret"];

            return requestToken;
        }

        public OauthSessionToken GetAccessToken(OauthRequestToken requestToken)
        {
            restClient.Authenticator = OAuth1Authenticator.ForAccessToken(_sessionToken.ConsumerKey, _sessionToken.ConsumerSecret, requestToken.oauth_token, requestToken.oauth_token_secret, requestToken.oauth_verifier);

            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
            RestRequest restRequest = new RestRequest("oauth/access_token", Method.GET);
            IRestResponse irestResponse = restClient.Execute(restRequest);

            NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(irestResponse.Content);

            _sessionToken.oauth_token = queryString["oauth_token"];
            _sessionToken.oauth_token_secret = queryString["oauth_token_secret"];

            return _sessionToken;
        }

        public bool RenewAccessToken()
        {
            restClient.Authenticator = OAuth1Authenticator.ForAccessTokenRefresh(_sessionToken.ConsumerKey, _sessionToken.ConsumerSecret, _sessionToken.oauth_token, _sessionToken.oauth_token_secret, null);

            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
            RestRequest restRequest = new RestRequest("oauth/renew_access_token", Method.GET);
            IRestResponse irestResponse = restClient.Execute(restRequest);

            var contentString = irestResponse.Content;
            var result = contentString.Trim().Equals("Access Token has been renewed");

            return result;
        }

        public bool RevokeAccessToken()
        {
            restClient.Authenticator = OAuth1Authenticator.ForAccessTokenRefresh(_sessionToken.ConsumerKey, _sessionToken.ConsumerSecret, _sessionToken.oauth_token, _sessionToken.oauth_token_secret, null);

            restClient.BaseUrl = new Uri(_sessionToken.OauthBaseUrl);
            RestRequest restRequest = new RestRequest("oauth/revoke_access_token", Method.GET);
            IRestResponse irestResponse = restClient.Execute(restRequest);

            var contentString = irestResponse.Content;
            var result = contentString.Trim().Equals("Revoked Access Token");

            return result;
        }

        #region protected internal

        protected internal string GetAuthorizationCode(OauthRequestToken token)
        {
            var result = string.Empty;

            CookieContainer cookieContainer = new CookieContainer();
            var webClient = new BrowserProcessor(cookieContainer);

            string authorizeUrl = _sessionToken.AuthorizeUrl;

            string requestUrl = string.Format(authorizeUrl, _sessionToken.ConsumerKey, token.oauth_token);

            var requestUri = new Uri(requestUrl);

            var docFromGet = GetPage(webClient, requestUri, null, cookieContainer, "GET");

            //get consent
            NameValueCollection consentCollection;
            Uri consentUri;
            GetConsentInput(docFromGet, requestUri, out consentCollection, out consentUri);
            var docConsent = GetPage(webClient, consentUri, consentCollection, cookieContainer, "POST");

            //Remark: keep in mind that you can have a different page here, when you will encounter it code for it
            //Refactor: create a method to identify what page did you received from the server to skip or process that specific page

            //get continue
            NameValueCollection tradingCollection;
            Uri tradingUri;
            GetTradingInput(docConsent, consentUri, out tradingCollection, out tradingUri);
            var docTrading = GetPage(webClient, tradingUri, tradingCollection, cookieContainer, "POST");

            token.oauth_verifier = (Regex.Replace(docTrading.DocumentNode.SelectNodes(".//input")[0].Attributes["Value"].Value, @"\s+", ""));

            return token.oauth_verifier;
        }

        public string GetStringContent(string relativePath)
        {
            restClient.BaseUrl = new Uri(_sessionToken.ApiBaseUrl);
            restClient.Authenticator = OAuth1Authenticator.ForProtectedResource(_sessionToken.ConsumerKey, _sessionToken.ConsumerSecret, _sessionToken.oauth_token, _sessionToken.oauth_token_secret);

            RestRequest restRequest = new RestRequest(relativePath, Method.GET);

            IRestResponse irestResponse = restClient.Execute(restRequest);

            return irestResponse.Content;
        }

        #endregion

        #region private area
        private string Escape(string s)
        {
            var charsToEscape = new[] { "!", "*", "'", "(", ")" };
            var escaped = new StringBuilder(Uri.EscapeDataString(s));
            foreach (var t in charsToEscape)
            {
                escaped.Replace(t, Uri.HexEscape(t[0]));
            }
            return escaped.ToString();
        }

        private string GetUrlFromParameters(string requestUrl, Dictionary<string, string> sigParameters, string tokenSecret)
        {
            var result = string.Empty;

            string oauth_timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
            var oauth_nonce = Escape(Convert.ToBase64String(Encoding.ASCII.GetBytes(oauth_timestamp)));
            var parameters = new SortedDictionary<string, string>
                {
                    {"oauth_consumer_key", _sessionToken.ConsumerKey},
                    {"oauth_nonce", oauth_nonce},
                    {"oauth_signature_method", "HMAC-SHA1"},
                    {"oauth_timestamp", oauth_timestamp},
                    {"oauth_version", "1.0"}
                };

            foreach(var sigParameter in sigParameters)
            {
                parameters.Add(sigParameter.Key, sigParameter.Value);
            }

            var sigStringBuilder = new StringBuilder();
            foreach (var parameter in parameters)
            {
                sigStringBuilder.AppendFormat("&{0}={1}", parameter.Key, parameter.Value);
            }
            sigStringBuilder.Remove(0, 1);

            var sigBase = sigStringBuilder.ToString();

            var sigFull = "GET&" + Escape(requestUrl) + "&" + Escape(sigBase);
            var oauth_signature = GetOathSignature(_sessionToken.ConsumerSecret, tokenSecret, sigFull);
            var urlBase = requestUrl + "?" + sigBase + "&oauth_signature=" + oauth_signature;

            result = requestUrl + "?" + sigBase + "&oauth_signature=" + oauth_signature;


            return result;
        }

        private string GetOathSignature(string consumerSecret, string tokenSecret, string fullSignature)
        {
            var result = string.Empty;
            var key = Escape(consumerSecret) + "&" + (tokenSecret == null ? "" : Escape(tokenSecret));
            var signatureEncoding = new ASCIIEncoding();
            var keyBytes = signatureEncoding.GetBytes(key);
            var signatureBaseBytes = signatureEncoding.GetBytes(fullSignature);
            var signatureString = Convert.ToBase64String((new HMACSHA1(keyBytes)).ComputeHash(signatureBaseBytes));

            result = Escape(signatureString);

            return result;
        }

        private void GetTradingInput(HtmlDocument docFrom, Uri inUri, out NameValueCollection valueCollection, out Uri outUri)
        {
            valueCollection = new NameValueCollection();
            var formFrom = docFrom.DocumentNode.SelectNodes("//form")[0];
            var actionFrom = formFrom.Attributes["action"].Value;
            foreach (var formInput in formFrom.SelectNodes(".//input"))
            {
                var nameInput = formInput.Attributes["Name"].Value;
                string valueInput = (formInput.Attributes.Any(p => p.Name.ToLower() == "value")) ? formInput.Attributes["Value"].Value : "";
                if (nameInput == "submit" && valueInput=="Decline") continue;

                valueCollection[nameInput] = valueInput;
            }

            outUri = new Uri(inUri.AbsoluteUri.Substring(0, inUri.AbsoluteUri.Length - inUri.PathAndQuery.Length) + actionFrom);
        }

        private void GetConsentInput(HtmlDocument docFromGet, Uri requestUri, out NameValueCollection valueCollection, out Uri consentUri)
        {
            valueCollection = new NameValueCollection();
            var formFromGet = docFromGet.DocumentNode.SelectNodes("//form")[0];
            var actionFromGet = formFromGet.Attributes["action"].Value;
            foreach (var formInput in formFromGet.SelectNodes(".//input"))
            {
                var nameInput = formInput.Attributes["Name"].Value;
                string valueInput = (formInput.Attributes.Any(p => p.Name.ToLower() == "value")) ? formInput.Attributes["Value"].Value : "";
                if (nameInput == "USER") valueInput = _sessionToken._habitat.EtradeUser;
                if (nameInput == "PASSWORD") valueInput = _sessionToken._habitat.EtradePass;
                if (nameInput == "REMEMBER_MY_USER_ID") continue;
                
                valueCollection[nameInput] = valueInput;
            }

            consentUri = new Uri(requestUri.AbsoluteUri.Substring(0, requestUri.AbsoluteUri.Length - requestUri.PathAndQuery.Length) + actionFromGet);
        }

        public HtmlDocument GetPage(BrowserProcessor webClient, Uri requestUri, NameValueCollection valueCollection, CookieContainer cookieContainer, string method)
        {
            var responseString = string.Empty;
            if (method.ToUpper().Equals("GET"))
            {
                responseString = webClient.DownloadString(requestUri);
            }
            else // POST
            {
                responseString = webClient.UploadValues(requestUri, valueCollection);

            }

            HtmlDocument docGet = new HtmlDocument();
            docGet.LoadHtml(responseString);

            return docGet;
        }

        #endregion
    }
}
