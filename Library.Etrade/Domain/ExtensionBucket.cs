﻿using Library.Domain.TradePortal;
using System.Collections.Generic;

namespace Library.Etrade.Domain
{
    public partial class MarketQuotesBucket
    {
        public List<TradePortalStockData> ToStockData()
        {
            List<TradePortalStockData> datalist = new List<TradePortalStockData>();

            var data = new TradePortalStockData();

            data.Source = QuoteResponse.QuoteData[0];
            data.Ask = QuoteResponse.QuoteData[0].All.Ask;
            data.Bid = QuoteResponse.QuoteData[0].All.Bid;
            data.LastTradePrice = QuoteResponse.QuoteData[0].All.LastTrade;
            data.Symbol = QuoteResponse.QuoteData[0].Product.Symbol;

            datalist.Add(data);

            return datalist;
        }

    }
    public partial class OptionExpireDatesBucket
    {
        public List<TradePortalExpireDates> ToExpireDates()
        {
            var datalist = new List<TradePortalExpireDates>();

            foreach (var item in OptionExpireDateResponse.ExpirationDate)
            {
                var data = new TradePortalExpireDates();

                data.Source = item;
                data.ExpiryType = item.ExpiryType;
                data.TradeDate = item.Year.ToString("0000")
                    + item.Month.ToString("00")
                    + item.Day.ToString("00");

                datalist.Add(data);
            }

            return datalist;
        }
    }

    public partial class OptionChainsBucket
    {
        public List<TradePortalOptionChain> ToOptionChains()
        {
            var datalist = new List<TradePortalOptionChain>();

            foreach (var item in this.OptionChainResponse.OptionPair)
            {
                if (item.Call != null)
                {
                    var data = GetOptionChain(item.Call, OptionChainResponse.SelectedEd);

                    datalist.Add(data);
                }

                if (item.Put != null)
                {
                    var data = GetOptionChain(item.Put, OptionChainResponse.SelectedEd);

                    datalist.Add(data);
                }
            }

            return datalist;
        }

        private TradePortalOptionChain GetOptionChain(Call item, SelectedEd selectedEd)
        {
            var data = new TradePortalOptionChain();

            data.Source = item;
            data.Symbol = item.Symbol;
            data.Strike = item.StrikePrice;
            data.Ask = item.Ask;
            data.Bid = item.Bid;
            data.SetContractMonth(selectedEd.Year, selectedEd.Month, selectedEd.Day);
            data.Right = item.OptionType.Equals("CALL") ? "C" : "P";
            data.Provider = "Etrade";

            return data;
        }
    }

    public partial class AccountsBucket
    {
        public List<TradePortalAccount> ToAccountList()
        {
            var datalist = new List<TradePortalAccount>();

            foreach (var item in this.AccountListResponse.Accounts.Account)
            {
                var data = new TradePortalAccount();

                data.Source = item;
                data.AccountName = item.AccountName;

                datalist.Add(data);
            }

            return datalist;
        }
    }
}
