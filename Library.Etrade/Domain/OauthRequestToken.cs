﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Etrade.Domain
{
    public class OauthRequestToken
    {
        public string oauth_token;
        public string oauth_token_secret;
        public string oauth_callback_confirmed;
        public string oauth_verifier;
    }
}
