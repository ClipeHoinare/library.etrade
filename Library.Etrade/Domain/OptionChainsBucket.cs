﻿// <auto-generated />
//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Library.Etrade.Domain;
//
//    var optionChainsBucket = OptionChainsBucket.FromJson(jsonString);

namespace Library.Etrade.Domain
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    public partial class OptionChainsBucket
    {
        [JsonProperty("OptionChainResponse")]
        public OptionChainResponse OptionChainResponse { get; set; }
    }

    public partial class OptionChainResponse
    {
        [JsonProperty("OptionPair")]
        public List<OptionPair> OptionPair { get; set; }

        [JsonProperty("SelectedED")]
        public SelectedEd SelectedEd { get; set; }
    }

    public partial class OptionPair
    {
        [JsonProperty("Call")]
        public Call Call { get; set; }

        [JsonProperty("Put")]
        public Call Put { get; set; }
    }

    public partial class Call
    {
        [JsonProperty("optionCategory")]
        public string OptionCategory { get; set; }

        [JsonProperty("optionRootSymbol")]
        public string OptionRootSymbol { get; set; }

        [JsonProperty("timeStamp")]
        public long TimeStamp { get; set; }

        [JsonProperty("adjustedFlag")]
        public bool AdjustedFlag { get; set; }

        [JsonProperty("displaySymbol")]
        public string DisplaySymbol { get; set; }

        [JsonProperty("optionType")]
        public string OptionType { get; set; }

        [JsonProperty("strikePrice")]
        public long StrikePrice { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("bid")]
        public decimal Bid { get; set; }

        [JsonProperty("ask")]
        public decimal Ask { get; set; }

        [JsonProperty("bidSize")]
        public long BidSize { get; set; }

        [JsonProperty("askSize")]
        public long AskSize { get; set; }

        [JsonProperty("inTheMoney")]
        public string InTheMoney { get; set; }

        [JsonProperty("volume")]
        public long Volume { get; set; }

        [JsonProperty("openInterest")]
        public long OpenInterest { get; set; }

        [JsonProperty("netChange")]
        public decimal NetChange { get; set; }

        [JsonProperty("lastPrice")]
        public decimal LastPrice { get; set; }

        [JsonProperty("quoteDetail")]
        public Uri QuoteDetail { get; set; }

        [JsonProperty("osiKey")]
        public string OsiKey { get; set; }

        [JsonProperty("OptionGreeks")]
        public OptionGreeks OptionGreeks { get; set; }
    }

    public partial class OptionGreeks
    {
        [JsonProperty("rho")]
        public decimal Rho { get; set; }

        [JsonProperty("vega")]
        public decimal Vega { get; set; }

        [JsonProperty("theta")]
        public decimal Theta { get; set; }

        [JsonProperty("delta")]
        public decimal Delta { get; set; }

        [JsonProperty("gamma")]
        public decimal Gamma { get; set; }

        [JsonProperty("iv")]
        public decimal Iv { get; set; }

        [JsonProperty("currentValue")]
        public bool CurrentValue { get; set; }
    }

    public partial class SelectedEd
    {
        [JsonProperty("month")]
        public long Month { get; set; }

        [JsonProperty("year")]
        public long Year { get; set; }

        [JsonProperty("day")]
        public long Day { get; set; }
    }

    public partial class OptionChainsBucket
    {
        public static OptionChainsBucket FromJson(string json) => JsonConvert.DeserializeObject<OptionChainsBucket>(json, Library.Etrade.Domain.Converter.Settings);
    }
}
