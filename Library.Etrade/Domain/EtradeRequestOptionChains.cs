﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Etrade.Domain
{
    internal class EtradeRequestOptionChains
    {
        public string symbol;
        public string expiryYear;
        public string expiryMonth;
        public string expiryDay;
        public string includeWeekly;
        public string chainType;
        public string strikePriceNear;
        public string noOfStrikes;
        public string skipAdjusted;
        public string optionCategory;
        public string priceType;
    }
}
