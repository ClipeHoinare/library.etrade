﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Etrade.Domain
{
    public interface IService
    {
        Exception LastError { get; }
        bool Process(string parameters);
    }

    //private class EtradeAccountManager : IService
    //{
    //    private Exception _lastError;
    //    public Exception LastError { get => _lastError; }

    //    public bool Process(string parameters = null)
    //    {
    //        bool succesfully = true;

    //        return succesfully;
    //    }
    //}
}
