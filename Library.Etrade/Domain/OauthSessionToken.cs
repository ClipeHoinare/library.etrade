﻿//using Library.Domain.Configuration;
//using Library.Domain.TradePortal;
using Library.Domain.Core.Configuration;
using Library.Domain.TradePortal;
using System;

namespace Library.Etrade.Domain
{
    public class OauthSessionToken
    {
        public EtradeHabitatInfo _habitat { get; set; }
        public string ConsumerKey { set; get; }
        public string ConsumerSecret { set; get; }
        public string OauthBaseUrl { set; get; }
        public string ApiBaseUrl { set; get; }
        public string AuthorizeUrl { set; get; }

        public string oauth_token;
        public string oauth_token_secret;

        public OauthSessionToken(string habitatFlagString)
        {
            _habitat = new EtradeHabitatInfo(habitatFlagString);

            if (_habitat.HabitatFlag.Equals(TradePortalHabitat.HabitatFlag.Live))
            {
                ConsumerKey = ApplicationConfiguration.AppSettings.LiveConsumerKey;
                ConsumerSecret = ApplicationConfiguration.AppSettings.LiveConsumerSecret;

                ApiBaseUrl = _habitat.LiveApiBaseUrl;
                OauthBaseUrl = _habitat.OauthBaseUrl;
                AuthorizeUrl = _habitat.AuthorizeUrl;
            }
            else if (_habitat.HabitatFlag.Equals(TradePortalHabitat.HabitatFlag.PaperMoney))
            {
                //there is no sandbox defined for IB, hence we are throwing an error on this case
                throw new Exception(string.Format("No PaperMoney habitat define for ETrade"));
            }
            else if (_habitat.HabitatFlag.Equals(TradePortalHabitat.HabitatFlag.Sandbox))
            {
                ConsumerKey = ApplicationConfiguration.AppSettings.SandboxConsumerKey;
                ConsumerSecret = ApplicationConfiguration.AppSettings.SandboxConsumerSecret;

                ApiBaseUrl = _habitat.SandboxApiBaseUrl;
                OauthBaseUrl = _habitat.OauthBaseUrl;
                AuthorizeUrl = _habitat.AuthorizeUrl;
            }
            else
            {
                throw new Exception(string.Format("Not defined habitat: %0", _habitat.HabitatFlag.ToString()));
            }
        }

        public void Dispose()
        {
        }

        public bool HasCredentials()
        {
            return !(string.IsNullOrEmpty(oauth_token) && string.IsNullOrEmpty(oauth_token_secret));
        }

        public class EtradeHabitatInfo
        {
            public string EtradeUser = ApplicationConfiguration.AppSettings.EtradeUser;
            public string EtradePass = ApplicationConfiguration.AppSettings.EtradePass;

            // Authorization links
            public string OauthBaseUrl = "https://api.etrade.com/";
            public string AuthorizeUrl = "HTTPS://us.etrade.com/e/t/etws/authorize?key={0}&token={1}";

            //Accounts lists
            public string SandboxApiBaseUrl = " https://apisb.etrade.com/v1/";
            public string LiveApiBaseUrl = " https://api.etrade.com/v1/";


            internal TradePortalHabitat.HabitatFlag HabitatFlag { get; set; }

            public EtradeHabitatInfo(string habitatFlagString)
            {
                HabitatFlag = TradePortalHabitat.InterpretFlag(habitatFlagString);
            }
        }
    }
}
