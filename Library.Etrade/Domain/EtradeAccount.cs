﻿//using Library.Domain.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Etrade.Domain
{
    internal class EtradeItem
    {
        public string accountId;
        public string accountIdKey;
        public string accountMode;
        public string accountDesc;
        public string accountName;
        public string accountType;
        public string institutionType;
        public string accountStatus;
        public long closedDate;
    }
}
